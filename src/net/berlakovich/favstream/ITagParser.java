package net.berlakovich.favstream;

public interface ITagParser {
	public String getArtist(String taginfo);

	public String getTitle(String tagInfo);

}
