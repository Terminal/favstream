package net.berlakovich.favstream;

import java.util.Iterator;
import java.util.List;

public class OneWayUrlProber implements IUrlProber {

	private List<String> urls;
	private Iterator<String> urlIterator;
	private String currentUrl = null;

	public OneWayUrlProber(List<String> urls) {
		this.urls = urls;
		this.urlIterator = this.urls.iterator();
	}

	@Override
	public String getUrl() throws NoValidUrlException {
		if (currentUrl == null) {
			if (!this.urlIterator.hasNext()) {
				throw new NoValidUrlException();
			}

			currentUrl = this.urlIterator.next();
		}
		return this.currentUrl;
	}

	@Override
	public void invalidateCurrentUrl() {
		this.currentUrl = null;
	}

}
