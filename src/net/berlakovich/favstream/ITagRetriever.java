package net.berlakovich.favstream;

public interface ITagRetriever {
	public void startRetrieving();

	public void stopRetrieving();

	public void addTagChangeListener(ITagChangeListener listener);
}
