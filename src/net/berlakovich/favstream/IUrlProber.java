package net.berlakovich.favstream;

public interface IUrlProber {
	public String getUrl() throws NoValidUrlException;

	public void invalidateCurrentUrl();
}
