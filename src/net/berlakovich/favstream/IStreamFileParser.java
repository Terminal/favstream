package net.berlakovich.favstream;

import java.util.List;

public interface IStreamFileParser {
	public List<String> getUrls();

	public List<String> getTitles();
}
