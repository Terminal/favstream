package net.berlakovich.favstream;

public class NoValidUrlException extends Exception {
	public NoValidUrlException() {

	}

	public NoValidUrlException(String message) {
		super(message);
	}
}
