package net.berlakovich.favstream;

import java.util.Arrays;
import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class PlayerService extends Service implements ITagChangeListener
{

	public static final String URLS_EXTRA = "URLS_EXTRA";
	public static final String NEW_TAG = "PlayerService.new_tag";
	private static final String TAG = "PlayerService";

	private List<String> urls;
	private IUrlProber playbackUrlProber;
	private IUrlProber tagUrlProber;
	private MediaPlayerController mediaPlayerController;
	private PlsTagRetriever tagRetriever;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.d(TAG, "PlayerService got start command");
		Bundle extras = intent.getExtras();
		this.urls = extras.getStringArrayList(URLS_EXTRA);
		Log.d(TAG, "provided url data: " + this.urls);
		
		this.mediaPlayerController = new MediaPlayerController();
		this.playbackUrlProber = new OneWayUrlProber(this.urls);
		this.tagUrlProber = new RoundRobinUrlProber(this.urls);
		
		Log.d(TAG, "helper initialized, preparing mediaplayer");
		
		try {
			mediaPlayerController.initMediaPlayer(this.playbackUrlProber);
		} catch (NoValidUrlException e) {
			Toast errorMessage = Toast.makeText(getApplicationContext(),
					"No valid urls found", Toast.LENGTH_SHORT);
			errorMessage.show();
			this.stopSelf();
		}

		this.tagRetriever = new PlsTagRetriever(this.tagUrlProber, 5000);
		this.tagRetriever.addTagChangeListener(this);
		this.tagRetriever.startRetrieving();
		
		this.mediaPlayerController.startPlaying();
		
		
		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy()
	{
 		this.tagRetriever.stopRetrieving();
		this.mediaPlayerController.stopPlaying();
		super.onDestroy();
	}
	
	@Override
	public void onTagChanged(final String tagInfo)
	{
		Log.d(TAG, "received a tag info update: " + tagInfo);
		Intent tweetMessage = new Intent(NEW_TAG);
		tweetMessage.putExtra(Intent.EXTRA_TEXT, tagInfo);
		sendBroadcast(tweetMessage);
	}

	@Override
	public IBinder onBind(Intent arg0)
	{
		return null;
	}

}
