package net.berlakovich.favstream;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class MainTab extends Fragment
{
	private static final String TAG = "Main";
	private static final String SAVED_INTENT = "SAVED_INTENT";
	private static final String SAVED_PLAYING_STATE = "SAVED_PLAYING_STATE";
	private View fragmentView;
	private Intent startIntent;

	private ImageButton playButton;
	private ImageButton pauseButton;
	private Button saveStreamNameButton;

	private TextView txtArtist;
	private TextView txtTitle;

	private TextView marqStreamName;
	private EditText txtStreamName;
	private ViewSwitcher viewSwitcher;

	private BroadcastReceiver broadcastReceiver;
	private List<String> loadedStreamNames;
	private List<String> loadedUrls;
	private PlsTagParser tagParser;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// we got an intent pending from the controlling activity
		if (this.startIntent != null) {
			this.processOpenIntent(this.startIntent);
		} else {
			Intent savedIntent = savedInstanceState.getParcelable(SAVED_INTENT);
			if(savedIntent != null) {
				this.startIntent = savedIntent;
				this.processOpenIntent(savedIntent);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		super.onCreateView(inflater, container, savedInstanceState);
		this.fragmentView = inflater.inflate(R.layout.maintab, container, false);

		Log.i(TAG, "initializing main view");
		playButton = (ImageButton) this.fragmentView.findViewById(R.id.btnPlay);
		pauseButton = (ImageButton) this.fragmentView.findViewById(R.id.btnPause);
		txtArtist = (TextView) this.fragmentView.findViewById(R.id.txtArtist);
		txtTitle = (TextView) this.fragmentView.findViewById(R.id.txtTitle);
		marqStreamName = (TextView) this.fragmentView.findViewById(R.id.marqStreamName);
		txtStreamName = (EditText) this.fragmentView.findViewById(R.id.txtStreamName);
		saveStreamNameButton = (Button) this.fragmentView.findViewById(R.id.btnSaveStreamName);
		viewSwitcher = (ViewSwitcher) this.fragmentView.findViewById(R.id.viewSwStreamName);
		
		if(savedInstanceState == null) {
			pauseButton.setEnabled(false);
			saveStreamNameButton.setEnabled(false);
		} else {
			boolean isPlaying = savedInstanceState.getBoolean(SAVED_PLAYING_STATE);
			this.setControlButtonState(isPlaying);
		}

		saveStreamNameButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				saveStreamNameButton.setEnabled(false);
				marqStreamName.setText(txtStreamName.getText().toString());
				marqStreamName.setSelected(true);
				viewSwitcher.showNext();
			}
		});

		viewSwitcher.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				saveStreamNameButton.setEnabled(true);
				txtStreamName.setText(marqStreamName.getText().toString());
				viewSwitcher.showNext();
				txtStreamName.requestFocus();
			}
		});

		playButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.i(TAG, "play button pressed");
				Intent startPlaybackIntent = new Intent(getActivity(), PlayerService.class);
				startPlaybackIntent.putStringArrayListExtra(PlayerService.URLS_EXTRA, new ArrayList<String>(loadedUrls));
				getActivity().startService(startPlaybackIntent);
				setControlButtonState(false);
			}
		});

		pauseButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.i(TAG, "stop button pressed");
				Intent stopPlaybackIntent = new Intent(getActivity(), PlayerService.class);
				getActivity().stopService(stopPlaybackIntent);
				setControlButtonState(true);
			}
		});

		return this.fragmentView;
	}

	@Override
	public void onResume()
	{
		super.onResume();

		if (this.broadcastReceiver == null) {

			this.broadcastReceiver = new BroadcastReceiver()
			{
				@Override
				public void onReceive(Context context, Intent intent)
				{
					if (intent.getAction().equals(PlayerService.NEW_TAG)) {
						Bundle data = intent.getExtras();
						String tagInfo = data.getString(Intent.EXTRA_TEXT);
						updateTagInfo(tagInfo);
					}
				}

			};
		}
		
		IntentFilter intentFilter = new IntentFilter(PlayerService.NEW_TAG);
		getActivity().registerReceiver(broadcastReceiver, intentFilter);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		
		if(this.broadcastReceiver != null) {
			getActivity().unregisterReceiver(broadcastReceiver);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putParcelable(SAVED_INTENT, this.startIntent);
	}

	public void setStartingIntent(Intent intent)
	{
		Activity activity = this.getActivity();
		this.startIntent = intent;

		if (activity != null)
			this.processOpenIntent(this.startIntent);
	}

	private void updateTagInfo(final String tagInfo)
	{
		Log.d(TAG, "received a tag info update: " + tagInfo);
		// TODO: should be called from an AsyncTask
		txtArtist.setText(tagParser.getArtist(tagInfo));
		txtTitle.setText(tagParser.getTitle(tagInfo));
	}

	private void processOpenIntent(Intent intent)
	{
		Log.i(TAG, "going to processing intent");
		InputStream inputStream = null;
		if (intent.getScheme().equals("content")) {
			Log.i(TAG, "intent VIEW with scheme content detected");
			try {
				Log.i(TAG, "initializing pls file processing");
				inputStream = getActivity().getContentResolver().openInputStream(intent.getData());
				IStreamFileParser plsParser = new PlsParser(inputStream);
				loadedUrls = plsParser.getUrls();
				Log.d(TAG, "extracted urls from the pls file: " + loadedUrls);
				loadedStreamNames = plsParser.getTitles();
				Log.d(TAG, "extracted stream names from the pls file: " + loadedStreamNames);
				tagParser = new PlsTagParser();
			} catch (FileNotFoundException e) {
				Log.e(TAG, "the ressource uri contained in the intent could not be opened", e);
				showErrorDialog("The supplied file could not be found");
			}
		} else {
			Log.e(TAG, "the intent data could not be parsed");
			showErrorDialog("Unknown file format");
		}

	}

	private void setStreamName(String streamName)
	{
		marqStreamName.setText(streamName);
		marqStreamName.setSelected(true);
	}

	private void showErrorDialog(String errorMessage)
	{
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		dialogBuilder.setMessage(errorMessage);
		dialogBuilder.setNegativeButton("OK", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});

		AlertDialog dialog = dialogBuilder.create();
		dialog.show();
	}

	private void setControlButtonState(boolean isPlaying)
	{
		this.playButton.setEnabled(isPlaying);
		this.pauseButton.setEnabled(!isPlaying);
	}

}
