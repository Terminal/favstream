package net.berlakovich.favstream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import android.util.Log;

public class PlsParser implements IStreamFileParser {
	private final BufferedReader reader;
	private List<String> urls;
	private List<String> streamTitles;
	private static final String TAG = "PlsParser";

	public PlsParser(InputStream input) {
		this.reader = new BufferedReader(new InputStreamReader(input));
		this.urls = new LinkedList<String>();
		this.streamTitles = new LinkedList<String>();
		this.parseFile();
	}

	private void parseFile() {
		while (true) {
			try {
				String line = reader.readLine();
				if (line == null) {
					break;
				}

				if (line.toLowerCase(Locale.US).indexOf("file") != -1) {
					String url = this.getLineValue(line);

					if (url != null && !url.equals("")) {
						this.urls.add(url);
					}
				}
				if (line.toLowerCase(Locale.US).indexOf("title") != -1) {
					String title = this.getLineValue(line);

					if (title != null && !title.equals("")) {
						this.streamTitles.add(title);
					}
				}

			} catch (IOException e) {
				Log.e(TAG, "an error occurred while parsing the pls file", e);
			}
		}

	}

	private String getLineValue(String line) {
		if (line == null) {
			return null;
		}

		String[] parts = line.split("=");

		if (parts == null || parts.length < 2) {
			return null;
		}

		return parts[1].trim();
	}

	@Override
	public List<String> getUrls() {
		return this.urls;
	}

	@Override
	public List<String> getTitles() {
		return this.streamTitles;
	}
}
