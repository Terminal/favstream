package net.berlakovich.favstream;

import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.util.Log;

public class MediaPlayerController implements OnPreparedListener {
	private boolean mediaPlayerPrepared;
	private MediaPlayer mediaPlayer;
	private static final String TAG = "MediaPlayerController";

	public MediaPlayerController() {
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnPreparedListener(this);
	}

	public void initMediaPlayer(IUrlProber urlProber)
			throws NoValidUrlException {

		Log.d(TAG, "initializing mediaplayer with a streaming url");
		String url = null;
		while (true) {
			try {
				url = urlProber.getUrl();
				Log.d(TAG, "trying url " + url);
				mediaPlayer.setDataSource(url);
				break;
			} catch (IOException e) {
				Log.e(TAG, "URL " + url + " is invalid, trying next", e);
				urlProber.invalidateCurrentUrl();
			}
		}
	}

	public void startPlaying() {
		if (!this.mediaPlayerPrepared) {
			Log.d(TAG,
					"going to prepare mediaplayer before start can be performed");
			this.mediaPlayer.prepareAsync();
		} else {
			Log.d(TAG, "mediaplayer already prepared, starting");
			this.mediaPlayer.start();
		}
	}

	public void stopPlaying() {
		this.mediaPlayer.stop();
	}

	@Override
	public void onPrepared(MediaPlayer arg0) {
		Log.d(TAG, "MediaPlayer is ready");
		mediaPlayer.start();
	}

}
