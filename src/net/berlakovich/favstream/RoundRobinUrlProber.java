package net.berlakovich.favstream;

import java.util.Iterator;
import java.util.List;

public class RoundRobinUrlProber implements IUrlProber {

	private List<String> urls;
	private Iterator<String> urlIterator;
	private String currentUrl = null;

	public RoundRobinUrlProber(List<String> urls) {
		this.urls = urls;
		this.urlIterator = this.urls.iterator();
	}

	@Override
	public String getUrl() throws NoValidUrlException {
		if (this.currentUrl == null) {
			if (!this.urlIterator.hasNext()) {
				this.urlIterator = this.urls.iterator();
			}

			this.currentUrl = this.urlIterator.next();
		}

		return this.currentUrl;

	}

	@Override
	public void invalidateCurrentUrl() {
		this.currentUrl = null;

	}

}
