package net.berlakovich.favstream;

public interface ITagChangeListener {
	public void onTagChanged(String tagInfo);
}
