package net.berlakovich.favstream;

import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class TabManager<FragmentType extends Fragment> implements TabListener {
    private FragmentType managedFragment;
    private final String fragmentTag;
	private int containerId;
	private boolean added;
	private Class<FragmentType> fragmentClass;
	private Activity fragmentActivity;

    /** Constructor used each time a new tab is created.
      * @param activity  The host Activity, used to instantiate the fragment
      * @param tag  The identifier tag for the fragment
      * @param clz  The fragment's Class, used to instantiate the fragment
      */
    public TabManager(Activity activity, int container, String tag, Class<FragmentType> clz) {
        fragmentActivity = activity;
    	containerId = container;
        fragmentTag = tag;
        fragmentClass = clz;
        managedFragment = (FragmentType)activity.getFragmentManager().findFragmentByTag(tag);
    }

    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        // Check if the fragment is already initialized
        if (managedFragment == null) {
            // If not, instantiate it and add it to the activity
        	managedFragment = (FragmentType)Fragment.instantiate(fragmentActivity, fragmentClass.getName());
            ft.add(containerId, managedFragment, fragmentTag);
        } else {
            // If it exists, simply attach it in order to show it
        	if(managedFragment.isDetached()) {
        		ft.attach(managedFragment);
        	}
        }
    }

    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        if (managedFragment != null) {
            // Detach the fragment, because another one is being attached
            ft.detach(managedFragment);
        }
    }

    public void onTabReselected(Tab tab, FragmentTransaction ft) {
        // User selected the already selected tab. Usually do nothing.
    }
    
    public FragmentType getManagedFragment() 
    {
    	return this.managedFragment;
    }
}
