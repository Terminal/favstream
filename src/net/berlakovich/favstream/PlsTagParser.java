package net.berlakovich.favstream;

public class PlsTagParser implements ITagParser {

	@Override
	public String getArtist(String tagInfo) {
		return getTagPartsField(tagInfo, 1);
	}

	@Override
	public String getTitle(String tagInfo) {
		return getTagPartsField(tagInfo, 2);
	}

	private String getTagPartsField(String tagInfo, int tagPart) {
		if (tagInfo != null) {
			tagInfo = tagInfo.replaceAll("_", " ");
			String[] tagParts = tagInfo.split("-");
			if (tagParts.length >= tagPart) {
				return tagParts[tagPart].trim();
			}
		}

		return null;
	}

}
