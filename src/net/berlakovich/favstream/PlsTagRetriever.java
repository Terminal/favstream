package net.berlakovich.favstream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class PlsTagRetriever implements ITagRetriever {

	private final class TagRetrieveTask extends AsyncTask<Void, Void, String> {
		private IUrlProber urlProber;
		private static final String TAG = "PlsTagRetrieverTask";

		
		private TagRetrieveTask(IUrlProber urlProber) {
			this.urlProber = urlProber;
		}

		@Override
		protected String doInBackground(Void... params) {

			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			String responseString = null;
			String nextUrl;
			
			try {
				nextUrl = this.urlProber.getUrl();
			} catch (NoValidUrlException e1) {
				Log.e(TAG, "no valid tag url could be found", e1);
				return null;
			}

			Log.d(TAG, "trying to retrieve tag info from " + nextUrl);
			
			URL tagUrl = null;

			try {
				URL baseUrl = new URL(nextUrl);
				tagUrl = new URL(baseUrl, "title");
			} catch (MalformedURLException e) {
				Log.e(TAG, "unable to construct pls title info url", e);
				return null;
			}

			Log.d(TAG, "Constructed tag url" + nextUrl);
			
			HttpUriRequest request = new HttpGet(tagUrl.toString());

			// fake the user agent, otherwise the request is rejected by ICY
			request.setHeader("User-Agent", "Mozilla/5.0");
			try {
				response = httpclient.execute(request);
				StatusLine statusLine = response.getStatusLine();
				if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();

					Pattern regexPattern = Pattern
							.compile(".*<body>(.+)</body>.*");
					Matcher regexMatcher = regexPattern.matcher(responseString);
					if (regexMatcher.find()) {
						return regexMatcher.group(1);
					}
					
					Log.d(TAG, "unable to find tag info in the returned body");

				} else {
					// Closes the connection.
					response.getEntity().getContent().close();
					Log.e(TAG, "received an invalid http status code: " + statusLine);
					return null;
				}
			} catch (ClientProtocolException e) {
				this.urlProber.invalidateCurrentUrl();
				Log.e(TAG, "a client protocol exception occurred: " + e);
				return null;
			} catch (IOException e) {
				this.urlProber.invalidateCurrentUrl();
				Log.e(TAG, "an io exception occurred: " + e);
				return null;
			}
			this.urlProber.invalidateCurrentUrl();
			return null;
		}
		
		@Override
		protected void onPostExecute(String result)
		{
			fireTagChangedEvent(result);
		}
	}

	private List<ITagChangeListener> listeners;
	private IUrlProber urlProber;
	private int retrieveInterval;
	private Timer taskTimer;
	private static final String TAG = "PlsTagRetriever";

	public PlsTagRetriever(IUrlProber urlProber, int interval) {
		this.listeners = new LinkedList<ITagChangeListener>();
		this.urlProber = urlProber;
		this.retrieveInterval = interval;
		this.taskTimer = new Timer();
	}

	@Override
	public void addTagChangeListener(ITagChangeListener listener) {
		if (listener == null) {
			throw new InvalidParameterException();
		}

		this.listeners.add(listener);
	}

	private void fireTagChangedEvent(String tagInfo) {
		for (ITagChangeListener listener : this.listeners) {
			listener.onTagChanged(tagInfo);
		}
	}

	@Override
	public void startRetrieving() {
		Log.d(TAG, "starting an url prober task");
		taskTimer.scheduleAtFixedRate(new TimerTask()
		{
			@Override
			public void run()
			{
				AsyncTask<Void, Void, String> updateTask = new TagRetrieveTask(urlProber);
				updateTask.execute();
			}
		}, 0, this.retrieveInterval);
	}

	@Override
	public void stopRetrieving() {
		Log.d(TAG, "canceling url prober task");
		taskTimer.cancel();
		taskTimer = new Timer();
	}

}
