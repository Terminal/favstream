package net.berlakovich.favstream;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Main extends Activity implements ITagChangeListener {

	private static final String TAG = "Main";
	private TabManager<MainTab> playerTabManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main);
		
		// TODO: only recreate the fragments if neccessary
		
	    // setup action bar for tabs
	    ActionBar actionBar = getActionBar();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	    actionBar.setDisplayShowTitleEnabled(false);

	    playerTabManager = new TabManager<MainTab>(
		        this, R.id.fragmentContainer, "player", MainTab.class);
		Tab playerTab = actionBar.newTab()
	            .setText("Player")
	            .setTabListener(playerTabManager);
	    actionBar.addTab(playerTab);
	    
	    Intent intent = this.getIntent();
	    
		if (intent.getAction() == Intent.ACTION_VIEW && savedInstanceState == null) {
			actionBar.selectTab(playerTab);
			
			MainTab playerFragment = playerTabManager.getManagedFragment();
			playerFragment.setStartingIntent(intent);
		}
	}

	@Override
	public void onTagChanged(String tagInfo)
	{
		// TODO: Call history fragment
	}

}
